<?php declare(strict_types=1);

namespace App;

use Laravel\Lumen\Application as ParentApplication;

class Application extends ParentApplication
{
    /**
     * Set the error handling for the application.
     *
     * Differences from Lumen: doesn't throw exceptions when
     * errors occur.
     *
     * @return void
     */
    protected function registerErrorHandling()
    {
        error_reporting(-1);

        set_exception_handler(function ($e) {
            $this->handleUncaughtException($e);
        });
        register_shutdown_function(function () {
            $this->handleShutdown();
        });
    }
}
