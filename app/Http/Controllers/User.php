<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Adduc\Stitcher\Client;
use Adduc\Stitcher\Password;
use DateTime;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Request;

class User extends Controller
{
    public function login(Request $request, Client $client, Password $password)
    {
        // @todo Switch to a session object through DI
        session_start();

        if ($request->isMethod('post')) {
            $device_id = bin2hex(openssl_random_pseudo_bytes(32));
            $email = $request->input('email');
            $password = $password->encrypt($device_id, $request->input('password'));

            $result = $client->CheckAuthentication([
                'email' => $email,
                'udid' => $device_id,
                'epx' => $password,
            ]);

            if ($result->error) {
                return response("Email and/or password is incorrect.", 403);
            }

            $bypassed_users = explode(",", env('BYPASS_USER_ID')) ?: [];

            if ($result->subscriptionState != 3 && !in_array($result->id, $bypassed_users)) {
                $function = env('APP_DEBUG', false) ? 'trigger_error' : 'error_log';
                $view = view('nonsubscriber', ['result' => $result]);
                return response($view, 403);
            }

            /** @todo move out of controller. */
            $sql = 'select * from users where stitcher_id = :id';
            $data = ['id' => $result->id];
            $user = app('db')->select($sql, $data);

            $expiration = new DateTime($result->subscriptionExpiration);

            if ($user) {
                $user = (array)$user[0];
                $user['expiration'] = new DateTime($user['expiration']);
                $user['updated_at'] = new DateTime();

                // If expiration has changed, update user details
                if ($expiration > $user['expiration']) {
                    $user['expiration'] = $expiration;
                    app('db')
                        ->table('users')
                        ->where('id', $user['id'])
                        ->update($user);
                }
            } else {
                $user = [
                    'stitcher_id' => $result->id,
                    'expiration' => $expiration,
                    'rss_user' => random_int(10000, 99999),
                    'rss_password' => random_int(10000, 99999),
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime(),
                ];

                $user['id'] = app('db')->table('users')->insertGetId($user);
            }

            $_SESSION['user'] = $user;
        }

        if (!empty($_SESSION['user'])) {
            return redirect('/search');
        }

        return view('login');
    }

    public function logout()
    {
        session_start();
        session_destroy();
        return redirect('/');
    }
}
