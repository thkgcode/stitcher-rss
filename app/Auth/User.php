<?php declare(strict_types=1);

namespace App\Auth;

use Adduc\Stitcher\Client;

class User
{
    /** @var int */
    public $id;

    /** @var int */
    public $stitcher_id;

    /** @var DateTime */
    public $expiration;

    /** @var string */
    public $rss_user;

    /** @var string */
    public $rss_password;

    /** @var string */
    public $created_at;

    /** @var DateTime */
    public $updated_at;

    /** @var Client */
    protected $client;

    public function __construct(Client $client, array $data = [])
    {
        $this->client = $client;

        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function hasPremium(): bool
    {
        if (!$this->stitcher_id) {
            return false;
        }

        if ($this->expiration > new \DateTime()) {
            return true;
        }

        $bypass_user = env('BYPASS_USER_ID');

        if ($bypass_user && $bypass_user == $this->stitcher_id) {
            return true;
        }

        // Cached for five minutes
        $result = $this->client->GetSubscriptionStatus([
            'uid' => $this->stitcher_id,
            'guzzle_options' => [
                'cache_ttl' => 60 * 30 // 30 minutes
            ]
        ]);

        if (!$result->subscriptionExpiration) {
            return false;
        }

        $expiration = new \DateTime(
            $result->subscriptionExpiration,
            new \DateTimeZone('America/Los_Angeles')
        );

        if ($expiration == $this->expiration) {
            return false;
        }

        $this->expiration = $expiration;

        $user = get_object_vars($this);
        unset($user['client']);

        app('db')
            ->table('users')
            ->where('id', $this->id)
            ->update($user);

        $now = new \DateTime();

        return ($this->expiration >= $now);
    }
}
