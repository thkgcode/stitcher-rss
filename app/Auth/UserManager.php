<?php declare(strict_types=1);

namespace App\Auth;

use Illuminate\Http\Request;
use Adduc\Stitcher\Client;

class UserManager
{
    /** @var Client */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function resolve(Request $request): ?User
    {
        $user
            = $this->resolveFromBasicAuth($request)
            ?? $this->resolveFromRoute($request)
            ?? $this->resolveFromSession($request);

        if ($user) {
            $user = new User($this->client, $user);
        }

        return $user;
    }

    public function resolveFromSession(Request $request): ?array
    {
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }

        if (empty($_SESSION['user'])) {
            return null;
        }

        return $_SESSION['user'];
    }

    public function resolveFromBasicAuth(Request $request): ?array
    {
        $user = $request->getUser();
        $pass = $request->getPassword();

        return $this->getUser($user, $pass);
    }

    public function resolveFromRoute(Request $request): ?array
    {
        $user = $request->route()[2]['rss_user'] ?? null;
        $pass = $request->route()[2]['rss_pass'] ?? null;

        return $this->getUser($user, $pass);
    }

    protected function getUser($user, $pass): ?array
    {
        if (!$user || !$pass) {
            return null;
        }

        $users = app('db')->select(
            'select * from users where rss_user = ?',
            [$user]
        );

        if (!$users || $users[0]->rss_password != $pass) {
            return null;
        }

        return get_object_vars($users[0]);
    }
}
