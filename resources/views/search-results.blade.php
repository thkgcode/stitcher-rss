<?php declare(strict_types=1); ?>

@extends('layout')
@inject('user', 'App\Auth\User')

<?php
$protocol = !empty($_SERVER['HTTPS']) ? 'https' : 'http';
$base_url = "://{$user->rss_user}:{$user->rss_password}@" . $_SERVER['HTTP_HOST'];
?>

@section('content')
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter has-text-centered">
        <form method="POST">
            <div class="field has-addons">
                <div class="control is-expanded">
                    <input class="input" name="term" type="text" placeholder="Find a show">
                </div>
                <div class="control">
                    <input type="submit" value="Search" class="button is-primary">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter">
    To add to your podcast client, copy the link, and paste into
        your podcast client without modification.
    </div>
</div>
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter">
        <div class="notification is-warning">
            Prompted for credentials? For your security, <strong>do not
                </strong> enter your Stitcher email/password into your
                podcast app. Use these generated numbers instead
                (created especially for you):
            <br>
            <br>
            <div style="font-family: monospace">
                Username: <strong>{{ $user->rss_user }}</strong><br>
                Password: <strong>{{ $user->rss_password }}</strong>
            </div>
        </div>
    </div>
</div>
<div class="columns is-desktop">
</div>
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter has-text-centered">
        @foreach ($response->feeds as $feed)
        <?php $feed_url = "/shows/" . ($feed->id_RSSFeed_premium ?: $feed->id) . "/feed"; ?>
        <div class="box">
        <article class="media">
            <div class="media-left">
            <figure class="image is-82x82">
                <img src="{{
                    str_replace(
                        'http://cloudfront.assets.stitcher.com',
                        'https://s3.amazonaws.com/stitcher.assets',
                        $feed->smallThumbnailURL
                    )
                }}" alt="Image">
            </figure>
            </div>
            <div class="media-content">
            <div class="content">
                <p>
                <strong>{{ $feed->name }}</strong>
                <br>
                <input class='feed-url' value="<?=$protocol . $base_url . $feed_url?>">
                </p>
            </div>
            <a class="button is-small is-primary is-outlined copy-feed">
                Copy Feed URL
            </a>
            <a class="button is-small is-primary is-outlined" href="itpc{{ $base_url . $feed_url }}">
                Subscribe
            </a>
            </div>
        </article>
        </div>
        @endforeach
    </div>
</div>

<script>
    (function() {
        "use strict";

        var clip = function () {
            var el = this.parentElement.querySelector('.feed-url');
            el.select();

            try {
                var successful = document.execCommand('copy');
                el.blur();
                alert("Copied to clipboard.");
            } catch (err) {}

        };

        document.querySelectorAll('.copy-feed').forEach(function (item, idx) {
            item.addEventListener('click', clip);
        });
    })();
</script>
@endsection
